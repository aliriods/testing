class Post < ApplicationRecord
  has_one_attached :photo

  def palindrome
    "#{title}#{title.reverse}"
  end

  def palindrome?
    title.downcase == title.reverse.downcase
  end

  def largest_palindrome
    arr = title.downcase.chars
    title.length.downto(1) do |n|
      ana = arr.each_cons(n).find { |b| b == b.reverse }
      return ana.join if ana
    end
  end

end
